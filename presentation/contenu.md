<!-- .slide: class="slide" -->
# Le programme EIG

Le principe du programme : intégrer pour 10 mois des profils numériques d'exception dans les administrations pour relever des défis d'amélioration du service public à l'aide du numérique et des données.

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->
<!-- .slide: class="slide" -->
# Le défi qu'on souhaite relever avec vous

Création d'une plateforme de traitement de données basée sur les technologies cloud (containerisation, stockage orienté objet..)

Bénéfices attendues :
* formations et montée en compétence des agents 
* création d'une communauté de pratiques
* accélerer la mise à disposition d'infrastructure en libre accès.

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->
<!-- .slide: class="slide" -->
# Pourquoi postuler au programme ?

* confronter nos idées à un regard externe pour juger de la pertinance du projet
* apporter de la visibilité au projet (DBnomics, Banque de France ... mais aussi des commerciaux)
* diversifier nos compétences avec des profils rares (design de services, Data engineer) et un peu moins rare (developpeur JS/ full stack)

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->
<!-- .slide: class="slide" -->
# Calendrier

![](https://d33wubrfki0l68.cloudfront.net/add5b0d289c7859a1b104691a8d3eb9529d19af5/bbadd/img/frise-programme.png)


----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->
<!-- .slide: class="slide" -->
# Deux versants à notre projet

- le [projet opensource](https://github.com/InseeFrLab/onyxia-ui)
- l'[instantiation de ce projet](https://spyrales.sspcloud.fr)  pour le service statistique publique

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->
<!-- .slide: class="slide" -->
# Conception générale

- data : datalake basé sur S3
- des logiciels de traitement instanciable à la volée basé sur les technologies de containerisation

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->
<!-- .slide: class="slide" -->
# la démo

 [spyrales](https://spyrales.sspcloud.fr)

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->
<!-- .slide: class="slide" -->
# Beaucoup de perpectives d'amélioration 

- organisation de la donnée pour guider l'utilisateur
- orchestration des workflows


