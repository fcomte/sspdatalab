# Content of the presentation
Presentation of the Insee innovation platform for the Bercy dataday (2019, November 29). 

:arrow_forward: [Slides](https://fcomte.gitlab.io/sspdatalab/#/)

For obtaining a printable PDF, open the following link in Chrome browser and print with PDFcreator.

:arrow_forward: [Printable version](https://fcomte.gitlab.io/sspdatalab/index.html?print-pdf#/)